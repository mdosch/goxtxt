/* Copyright 2018 Martin Dosch

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

// Package twtxt provides an interface to a local installation
// of the official twtxt client or txtnish.
package twtxt

import (
	"log"
	"os/exec"
	"strings"
)

var twtxtpath, txtnish = initTwtxt()

// Gets the path to twtxt or txtnish binary. Txtnish is prefered.
func initTwtxt() (string, bool) {
	txtnishPresent := true
	output, err := exec.LookPath("txtnish")
	if err != nil {
		output, err := exec.LookPath("twtxt")
		if err != nil {
			log.Fatal("neither txtnish nor twtxt found.")
		}

		txtnishPresent = false
		return output, txtnishPresent
	}
	return output, txtnishPresent
}

// Tweet sends a tweet.
// It returns twtxt output and any error encountered.
func Tweet(s []string) (string, error) {
	var command string
	for i, tweet := range s {
		command = command + tweet
		if i < len(s)-1 {
			command = command + " "
		}
	}
	out, err := exec.Command(twtxtpath, "tweet", command).Output()
	outputstring := string(out)
	return outputstring, err
}

// Timeline shows the requested amount of timeline entries.
// It returns the requested timeline entries and any
// error encountered.
func Timeline(entries int) (string, error) {
	var outputstring string
	var i int

	out, err := exec.Command(twtxtpath, "timeline").Output()
	if err == nil {
		outputlines := strings.Split(string(out), "\n")

		lines := entries*3 - 1

		if len(outputlines) < entries*3 {
			lines = len(outputlines)
		}

		for i = 0; i < lines; i++ {
			outputstring = outputstring + outputlines[i]
			if i < lines-1 {
				outputstring = outputstring + "\n"
			}
		}
	}
	return outputstring, err
}

// ViewUser shows the requested amount of timeline entries for
// the specified user.
// It returns the requested timeline entries and any
// error encountered.
func ViewUser(i int, user string) (string, error) {
	var outputstring string
	var k, m int

	if txtnish {

		out, err := exec.Command(twtxtpath, "timeline").Output()
		if err == nil {
			outputlines := strings.Split(string(out), "\n")
			for k = 0; k < len(outputlines); k++ {
				if strings.Contains(outputlines[k], "* "+user) {
					if m > 0 {
						outputstring = outputstring + "\n\n"
					}
					outputstring = outputstring + outputlines[k] + "\n" + outputlines[k+1]
					m++
				}
				if m == i-1 {
					break
				}
			}
		}
		return outputstring, err
	}

	out, err := exec.Command(twtxtpath, "timeline").Output()
	if err == nil {
		outputlines := strings.Split(string(out), "\n")
		for k = 0; k < len(outputlines); k++ {
			if strings.Contains(outputlines[k], "➤ "+user) {
				if m > 0 {
					outputstring = outputstring + "\n\n"
				}
				outputstring = outputstring + outputlines[k] + "\n" + outputlines[k+1]
				m++
			}
			if m == i-1 {
				break
			}
		}
	}
	return outputstring, err
}

// UserManagement follows or unfollows the specified user.
// It returns twtxt output and any error encountered.
func UserManagement(follow bool, s []string) (string, error) {
	var out []byte
	var err error

	if follow {
		out, err = exec.Command(twtxtpath, "follow", s[0], s[1]).Output()
	} else {
		out, err = exec.Command(twtxtpath, "unfollow", s[0]).Output()
	}

	outputstring := string(out)
	return outputstring, err
}

// ListFollowing lists the users you are following.
// It returns twtxt output and any error encountered.
func ListFollowing() (string, error) {
	out, err := exec.Command(twtxtpath, "following").Output()
	outputstring := string(out)
	return outputstring, err
}

// Mentions shows the requested amount of @-mentions for the specified user.
// It returns the requested timeline entries and any
// error encountered.
func Mentions(nick string, number int) (string, error) {
	var k, m int
	var outputstring string

	out, err := exec.Command(twtxtpath, "timeline").Output()
	if err == nil {
		outputlines := strings.Split(string(out), "\n")
		for k = 0; k < len(outputlines); k++ {
			if strings.Contains(outputlines[k], "@"+nick) {
				if m > 0 {
					outputstring = outputstring + "\n\n"
				}
				outputstring = outputstring + outputlines[k-1] + "\n" + outputlines[k]
				m++
			}
			if m == number-1 {
				break
			}
		}
	}
	return outputstring, err
}

// Tags shows the requested amount of #-tags.
// It returns the requested timeline entries and any
// error encountered.
func Tags(tag string, number int) (string, error) {
	var k, m int
	var outputstring string

	out, err := exec.Command(twtxtpath, "timeline").Output()
	if err == nil {
		outputlines := strings.Split(string(out), "\n")
		for k = 0; k < len(outputlines); k++ {
			if strings.Contains(strings.ToLower(outputlines[k]), "#"+tag) {
				if m > 0 {
					outputstring = outputstring + "\n\n"
				}
				outputstring = outputstring + outputlines[k-1] + "\n" + outputlines[k]
				m++
			}
			if m == number-1 {
				break
			}
		}
	}
	return outputstring, err
}
